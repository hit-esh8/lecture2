names = "Bob", "Ellen", "Rick", "Zelda"
names.sort()
vowel_names = []
counter = 0
for name in names:
    counter += 1
    if name[0].lower() in "aeiou":
        vowel_names.append(name) # new list
        print name + " starts with a vowel"
    else:
        print name + " starts with a consonant"

print vowel_names
print counter

sentence = ""
for name in names:
    sentence = sentence + " " + name

print sentence

sentence2 = "Four score and seven years ago"
sentencewithoutvowels = ""
for letter in sentence2:
    if letter not in "AEIOUaeiou":
        sentencewithoutvowels = sentencewithoutvowels + letter

print sentencewithoutvowels