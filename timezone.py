# datetime is a module for manipulating and displaying time.
import datetime

now = datetime.datetime.utcnow()
# This string describes the format to use for printing times:
#       Mon Oct 08 12:00 PM
format = "%a %b %d %I:%M %p"

print "Right now it is:"

# Use timedeltas to add and subtract times.
time = now + datetime.timedelta(hours=11)
print time.strftime(format), "Melbourne (UTC + 11)"

time = now + datetime.timedelta(hours=5.5)
print time.strftime(format), "India (UTC + 5.5)"
