def square(x):
    return x * x

def temperature(temp):
    if temp >= 40:
        print("Too hot")
    elif temp < 40 and temp >= 18:
        print("Just right!")
    elif temp > 10 and temp < 18:
        print("Too Cold")
    elif temp < 10:
            print("Extreme")
    else:
        pass

x = input()
temperature(x)

def main():
    for i in range(1, 11):
        print("{} squared is {}".format(i, square(i)))

    for i in range(5):
        print(f"{i} squared is {square(i)}")

if __name__ == "__main__":
    main()