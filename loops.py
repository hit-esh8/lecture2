for i in range(5):
    print(i)

names = ["Alice", "Bob", "Charlie", "Dingu"]
for name in names:
    print(f"Hello, {name}!")

s = set()
s.add(1)
s.add(3)
s.add(5)
s.add(3)
print(s)

if 2 in s:
    print(True)
else:
    print(False)

ages = {"Alice": 22, "Bob": 27}
ages["Charlie"] = 30
ages["Alice"] += 1

print(ages)

marks = {1: 85, 2: 67}
marks[3] = 92
marks[2] += 1

for stud in marks:
    print(f"Student Roll No {stud}, Marks: {marks[stud]}")
