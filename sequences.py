name = "Alice"
coordinates= (10, True, "ff")
names = ["Alice", "Bob", "Charlie"]

print(f"name is {name} of type {type(name)}")
print(f"coordinates is {coordinates} of type {type(coordinates)}")
print(f"names are {names} of type {type(names)}")
print(f"Length of {name} is {len(name)}")
print(f"Third letter in {name} is {name[2]}")