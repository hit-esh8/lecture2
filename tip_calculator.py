# sys is a module. It lets us access command line arguments,
import sys
if len(sys.argv) < 2:
    print "Please supply the total, without a leading '$' (example 56.78)"
    exit(1)

total = float(sys.argv[1])

for tip_amount in [.1, .15, .2]:
    tip = tip_amount * total
    print "%.1f percent : tip $%.2f, total $%.2f" % (tip_amount * 100, tip, total + tip)
