capitals_dict = {
'Karnataka' : 'Bangalore',
'Maharashtra' : 'Bombay',
'Andhra Pradesh' : 'Hyderabad',
'Bihar' : 'Patna',
'Haryana' : 'Chandigarh',
'Himachal Pradesh' : 'Shimla',
'Gujarat' : 'Gandhinagar',
'Goa' : 'Panaji',
'Arunachal Pradesh' : 'Itanagar',
'Wyoming' : 'Cheyenne',
'Wisconsin' : 'Madison',
'Assam' : 'Dispur',
'Jammu & Kashmir' : 'Srinagar',
'Kerala' : 'Thiruvananthapuram',
'Madhya Pradesh' : 'Bhopal',
'Manipur' : 'Imphal',
}

import random

states = capitals_dict.keys()
states_already_appeared = []
counter = 0
while counter < 5:
    state = random.choice(states)
    if state not in states_already_appeared:
        capital = capitals_dict[state]
        capital_guess = raw_input("What is the capital of " + state + "? ")

        if capital_guess == capital:
            print "Correct! Nice job."
        else:
            print "Incorrect. The capital of " + state + " is " + capital + "!"
    counter += 1
    states_already_appeared.append(state)
print "All done"